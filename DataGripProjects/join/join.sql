use joindb
--------- EXAMPLES -----------
SELECT * FROM Buyers

SELECT * FROM Sales

SELECT * FROM Produce

SELECT buyer_name, sales.buyer_id, qty
FROM buyers, sales
WHERE buyers.buyer_id = sales.buyer_id

SELECT buyer_name, s.buyer_id, qty
FROM buyers AS b, sales AS s
WHERE b.buyer_id = s.buyer_id

-------- JOINS --------

-- old school join - condition to join tables are given after where clause
SELECT buyer_name, b.buyer_id, qty
FROM buyers AS b, sales AS s
WHERE s.buyer_id = b.buyer_id

SELECT buyer_name, sales.buyer_id, qty
FROM buyers INNER JOIN sales
ON buyers.buyer_id = sales.buyer_id


use Northwind

SELECT productname, companyname
FROM products
INNER JOIN suppliers
ON products.supplierid = suppliers.supplierid

-- Napisz polecenie zwracające nazwy produktów i firmy je
-- dostarczające (baza northwind)
-- – tak aby produkty bez „dostarczycieli” i „dostarczyciele” bez
-- produktów nie pojawiali się w wyniku.
-- (ID tylko dla sprawdzenia)
SELECT ProductID, productname, companyname, Suppliers.SupplierID
FROM products
INNER JOIN suppliers
ON products.supplierid = suppliers.supplierid

-- Napisz polecenie zwracające jako wynik nazwy klientów, którzy
-- złożyli zamówienia po 01 marca 1998 (baza northwind)
SELECT DISTINCT companyname, orderdate
FROM orders
INNER JOIN customers
ON orders.customerid = customers.customerid
WHERE orderdate > '3/1/98'

-- OUTER JOIN --
use joindb
SELECT buyer_name, sales.buyer_id, qty
FROM buyers LEFT OUTER JOIN sales
ON buyers.buyer_id = sales.buyer_id

SELECT buyer_name, sales.buyer_id, qty
FROM buyers LEFT OUTER JOIN sales
ON buyers.buyer_id = sales.buyer_id
where Sales.buyer_id IS NULL

-- Napisz polecenie zwracające wszystkich klientów z datami
-- zamówień (baza northwind).
use Northwind

SELECT companyname, customers.customerid, orderdate
FROM customers
INNER JOIN orders
ON customers.customerid = orders.customerid

SELECT companyname, customers.customerid, orderdate
FROM customers
LEFT OUTER JOIN orders
ON customers.customerid = orders.customerid
WHERE Orders.OrderID IS NULL

-- Ćwiczenie
-- 1. Wybierz nazwy i ceny produktów (baza northwind) o cenie jednostkowej
-- pomiędzy 20.00 a 30.00, dla każdego produktu podaj dane adresowe dostawcy

select Products.ProductName, Products.UnitPrice
from Products inner join Suppliers
on Products.SupplierID = Suppliers.SupplierID
where Products.UnitPrice between 20 and 30
order by Products.UnitPrice

-- 2. Wybierz nazwy produktów oraz inf. o stanie magazynu dla produktów
-- dostarczanych przez firmę ‘Tokyo Traders’
select Products.ProductName, Products.UnitsInStock
from Products inner join Suppliers
on Products.SupplierID = Suppliers.SupplierID
where Suppliers.CompanyName = 'Tokyo Traders'


-- 3. Czy są jacyś klienci którzy nie złożyli żadnego zamówienia w 1997 roku, jeśli tak
-- to pokaż ich dane adresowe

select Customers.Address, Orders.OrderDate, Customers.CustomerID
from Customers left outer join Orders
on  Customers.CustomerID = Orders.CustomerID and Orders.OrderDate = '1997'
where Orders.OrderID is null

-- 4. Wybierz nazwy i numery telefonów dostawców, dostarczających produkty,
-- których aktualnie nie ma w magazynie

select Suppliers.CompanyName, Suppliers.Phone
from Suppliers left outer join Products
on  Suppliers.SupplierID = Products.SupplierID
where Products.UnitsInStock = 0

-- CROSS JOIN --
use joindb

SELECT buyer_name, qty
FROM buyers, sales


SELECT buyer_name, qty
FROM buyers
CROSS JOIN sales

-- Łączenie więcej niż dwóch tabel --
SELECT buyer_name, prod_name, qty
FROM buyers
INNER JOIN sales
ON buyers.buyer_id = sales.buyer_id
INNER JOIN produce
ON sales.prod_id = produce.prod_id

-- przykład --
use Northwind

SELECT orderdate, productname
FROM orders AS O
INNER JOIN [order details] AS OD
ON O.orderid = OD.orderid
INNER JOIN products AS P
ON OD.productid = P.productid
WHERE orderdate = '7/8/96'

-- Ćwiczenia
-- 1. Wybierz nazwy i ceny produktów (baza northwind) o cenie jednostkowej
-- pomiędzy 20.00 a 30.00, dla każdego produktu podaj dane adresowe dostawcy,
-- interesują nas tylko produkty z kategorii ‘Meat/Poultry’

select Products.ProductName, Products.UnitPrice, Suppliers.Address
from Products
    join Suppliers
        on Products.SupplierID = Suppliers.SupplierID
    join Categories
        on Products.CategoryID = Categories.CategoryID
where Products.UnitPrice between 20 and 30 and Categories.CategoryName = 'Meat/Poultry'

-- 2. Wybierz nazwy i ceny produktów z kategorii ‘Confections’ dla każdego produktu
-- podaj nazwę dostawcy.

select Products.ProductName, Products.UnitPrice, Suppliers.CompanyName
from Products
    join Suppliers
        on Products.SupplierID = Suppliers.SupplierID
    join Categories
        on Products.CategoryID = Categories.CategoryID
where Categories.CategoryName = 'Confections'

-- 3. Wybierz nazwy i numery telefonów klientów , którym w 1997 roku przesyłki
-- dostarczała firma ‘United Package’
select Customers.CompanyName, Customers.Phone, Orders.OrderDate,  Suppliers.CompanyName
from Customers
    join Orders
        on Customers.CustomerID = Orders.CustomerID and Orders.OrderDate = '1997'
    join [Order Details]
        on [Order Details].OrderID = Orders.OrderID
    join Products
        on [Order Details].ProductID = Products.ProductID
    join Suppliers
        on Products.SupplierID = Suppliers.SupplierID

-- where Suppliers.CompanyName = 'United Package'
-- ^ z where nie zwraca nikogo

-- 4. Wybierz nazwy i numery telefonów klientów, którzy kupowali produkty z kategorii
-- ‘Confections
select Customers.CompanyName, Customers.Phone
from Customers
    join Orders
        on Customers.CustomerID = Orders.CustomerID
    join [Order Details]
        on [Order Details].OrderID = Orders.OrderID
    join Products
        on [Order Details].ProductID = Products.ProductID
    join Categories
        on Products.CategoryID = Categories.CategoryID
where CategoryName = 'Confections'


-- 2. Napisz polecenie, które wyświetla listę dzieci będących członkami biblioteki (baza
-- library). Interesuje nas imię, nazwisko, data urodzenia dziecka, adres
-- zamieszkania dziecka oraz imię i nazwisko rodzica.
use library


select m.firstname, m.lastname, juvenile.birth_date, a.state, ma.firstname, ma.lastname
from juvenile
    join member m
        on juvenile.member_no = m.member_no
    join adult a
        on juvenile.adult_member_no = a.member_no
    join member ma
        on a.member_no = ma.member_no

-- Łączenie tabeli samej ze sobą – self join\
use joindb
SELECT a.buyer_id AS buyer1, a.prod_id, b.buyer_id AS buyer2
FROM sales AS a
    JOIN sales AS b
        ON a.prod_id = b.prod_id

-- 1. Napisz polecenie, które wyświetla pracowników oraz ich podwładnych (baza
-- northwind)
use Northwind

-- pomocnicze
select FirstName, LastName, ReportsTo
from Employees

select emp.FirstName, emp.LastName, boss.FirstName, boss.LastName
from Employees as emp
    join Employees as boss
        on emp.ReportsTo = boss.EmployeeID

select m.firstname, m.lastname, juvenile.birth_date, a.state, ma.firstname, ma.lastname
from juvenile
    join member m
        on juvenile.member_no = m.member_no
    join adult a
        on juvenile.adult_member_no = a.member_no
    join member ma
        on a.member_no = ma.member_no

-- Łączenie tabeli samej ze sobą – self join\
use joindb
SELECT a.buyer_id AS buyer1, a.prod_id, b.buyer_id AS buyer2
FROM sales AS a
    JOIN sales AS b
        ON a.prod_id = b.prod_id

-- 1. Napisz polecenie, które wyświetla pracowników oraz ich podwładnych (baza
-- northwind)
use Northwind

-- pomocnicze
select FirstName, LastName, ReportsTo
from Employees

select emp.FirstName, emp.LastName, boss.FirstName, boss.LastName
from Employees as emp
    join Employees as boss
        on emp.ReportsTo = boss.EmployeeID


-- CWICZENIA KONCOWE
-- 1. Dla każdego zamówienia podaj łączną liczbę zamówionych
-- jednostek towaru oraz
-- nazwę klienta.

select *
from Orders
inner join Customers on Orders.CustomerID = Customers.CustomerID
inner join [Order Details] on Orders.OrderID = [Order Details].OrderID

select Orders.OrderID, Customers.CompanyName, sum([Order Details].Quantity)
from Orders
inner join Customers on Orders.CustomerID = Customers.CustomerID
inner join [Order Details] on Orders.OrderID = [Order Details].OrderID
group by Orders.OrderID, Customers.CompanyName
order by  Orders.OrderID

-- 2. Napisz polecenie, które wyświetla pracowników, którzy nie mają podwładnych
-- (baza northwind)

select boss.FirstName, boss.LastName, emp.EmployeeID
from Employees as boss
    left outer join Employees as emp
        on emp.ReportsTo = boss.EmployeeID
where emp.EmployeeID is null

-- Łączenie kilku zbiorów wynikowych
SELECT (firstname + ' ' + lastname) AS name,city, postalcode
FROM employees
UNION
SELECT companyname, city, postalcode
FROM customers

-- 2. Napisz polecenie, które wyświetla pracowników, którzy nie mają podwładnych
-- (baza northwind)

select boss.FirstName, boss.LastName, emp.EmployeeID
from Employees as boss
    left outer join Employees as emp
        on emp.ReportsTo = boss.EmployeeID
where emp.EmployeeID is null

-- Łączenie kilku zbiorów wynikowych
SELECT (firstname + ' ' + lastname) AS name,city, postalcode
FROM employees
UNION
SELECT companyname, city, postalcode
FROM customers

use library

-- 4. Podaj listę członków biblioteki mieszkających w Arizonie (AZ) mają więcej niż
-- dwoje dzieci zapisanych do biblioteki

select member.member_no, member.firstname, member.lastname, count(*)
from member
    join adult a on member.member_no = a.member_no
    join juvenile j on a.member_no = j.adult_member_no
where a.state = 'AZ'
group by member.member_no, member.firstname, member.lastname
having count(*) >= 2

-- 1. Podaj listę członków biblioteki mieszkających w Arizonie (AZ) którzy mają więcej
-- niż dwoje dzieci zapisanych do biblioteki oraz takich którzy mieszkają w Kaliforni
-- (CA) i mają więcej niż troje dzieci zapisanych do biblioteki
select member.member_no, member.firstname, member.lastname, count(*)
from member
    join adult a on member.member_no = a.member_no
    join juvenile j on a.member_no = j.adult_member_no
where a.state = 'AZ'
group by member.member_no, member.firstname, member.lastname
having count(*) >= 2
union
select member.member_no, member.firstname, member.lastname, count(*)
from member
    join adult a on member.member_no = a.member_no
    join juvenile j on a.member_no = j.adult_member_no
where a.state = 'CA'
group by member.member_no, member.firstname, member.lastname
having count(*) >= 3

select member.member_no, member.firstname, member.lastname, count(*)
from member
    join adult a on member.member_no = a.member_no
    join juvenile j on a.member_no = j.adult_member_no
group by member.member_no, member.firstname, member.lastname, a.state
having (count(*) >= 2 and a.state = 'AZ') or (count(*) >= 3 and a.state = 'CA')
