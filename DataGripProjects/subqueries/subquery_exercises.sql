-- CW 1
-- 1. Wybierz nazwy i numery telefonów klientów , którym w 1997 roku przesyłki
-- dostarczała firma United Package.
select CompanyName, Phone
from Customers
where CustomerID in (   select Orders.CustomerID
                        from Orders join Shippers S
                        on Orders.ShipVia = S.ShipperID
                        where year(ShippedDate) = '1997' and S.CompanyName = 'United Package')

-- inna wersja
select CompanyName, Phone from Orders
join Customers on Orders.CustomerID = Customers.CustomerID
where year(ShippedDate) = 1997 and ShipVia = (select ShipperID from Shippers where CompanyName = 'United Package')

-- 2. Wybierz nazwy i numery telefonów klientów, którzy kupowali produkty z kategorii
-- Confections.

select CompanyName, Phone
from Customers
where CustomerID in (   select Orders.CustomerID
                        from Orders
                        join [Order Details] [O D] on Orders.OrderID = [O D].OrderID
                        join Products P on [O D].ProductID = P.ProductID
                        join Categories C on P.CategoryID = C.CategoryID
                        where C.CategoryName = 'Confections' )


-- 3. Wybierz nazwy i numery telefonów klientów, którzy nie kupowali produktów z
-- kategorii Confections.

select CompanyName, Phone
from Customers
where CustomerID not in (   select Orders.CustomerID
                            from Orders
                            join [Order Details] [O D] on Orders.OrderID = [O D].OrderID
                            join Products P on [O D].ProductID = P.ProductID
                            join Categories C on P.CategoryID = C.CategoryID
                            where C.CategoryName = 'Confections' )

-- CW 2
-- 1. Dla każdego produktu podaj maksymalną liczbę zamówionych jednostek
select ProductID, ProductName,
    (   select max(p_inner.Quantity)
        from [Order Details] as p_inner
        where p_inner.ProductID = p_outer.ProductID) as max_quantity
from Products as p_outer

-- 2. Podaj wszystkie produkty których cena jest mniejsza niż średnia cena produktu
-- select ProductID, ProductName, UnitPrice, (select avg(UnitPrice) from Products) as average
-- from Products

-- 3. Podaj wszystkie produkty których cena jest mniejsza niż średnia cena produktu
-- danej kategorii
