-- Podzapytania do tabel
SELECT T.orderid, T.customerid
FROM ( SELECT orderid, customerid
FROM orders ) AS T

-- Podzapytanie jako wyrażenie
SELECT productname, UnitPrice,
       ( SELECT AVG(UnitPrice) FROM products) AS average,
       UnitPrice-(SELECT AVG(UnitPrice) FROM products) AS difference
FROM products

-- Podzapytanie w warunku
SELECT productname, unitprice,
       ( SELECT AVG(unitprice) FROM products) AS average,
       unitprice-(SELECT AVG(unitprice) FROM products) AS difference
FROM products
WHERE unitprice > ( SELECT AVG(unitprice) FROM products)

-- Podzapytania skorelowane
SELECT productname, unitprice,
        (SELECT AVG(unitprice)
         FROM products as p_wew
         WHERE p_zew.categoryid = p_wew.categoryid ) AS average
FROM products as p_zew


-- Podzapytania skorelowane w warunku
SELECT productname, unitprice, CategoryID,
       (SELECT AVG(unitprice) FROM products as p_wew
        WHERE p_zewn.CategoryID = p_wew.categoryid ) AS average
FROM products as p_zewn
WHERE UnitPrice >
    (SELECT AVG(unitprice) FROM products as p_wew
     WHERE p_zewn.CategoryID = p_wew.categoryid )

-- skorelowane
SELECT DISTINCT productid, quantity
FROM [order details] AS ord1
WHERE quantity = (  SELECT MAX(quantity)
                    FROM [order details] AS ord2
                    WHERE ord1.productid =
                    ord2.productid )
ORDER BY ProductID


-- To samo przy użyciu GROUP BY
select productid, max(quantity)
from [order details]
group by productid
order by productid

-- Operatory EXISTS, NOT EXISTS
SELECT lastname, employeeid
FROM employees AS e
WHERE EXISTS (  SELECT * FROM orders AS o
                WHERE e.employeeid = o.employeeid
                AND o.orderdate = '9/5/97')

-- EXISTS vs JOIN
SELECT DISTINCT lastname, e.employeeid
FROM employees AS e
join Orders o on o.EmployeeID = e.EmployeeID
where o.OrderDate = '9/5/97'

-- Operatory EXISTS, NOT EXISTS
SELECT lastname, employeeid
FROM employees AS e
WHERE NOT EXISTS (SELECT * FROM orders AS o
WHERE e.employeeid = o.employeeid
AND o.orderdate = '9/5/97')

--join
SELECT DISTINCT lastname, e.employeeid
FROM employees AS e
left join Orders o on o.EmployeeID = e.EmployeeID
and o.OrderDate = '9/5/97'
where o.EmployeeID is null

-- Operatory IN, NOT IN
SELECT lastname, employeeid
FROM employees AS e
WHERE employeeid IN (  SELECT employeeid FROM orders AS o
                        WHERE o.orderdate = '9/5/97')

-- JOIN vs EXISTS vs IN
SELECT DISTINCT lastname, e.employeeid
FROM orders AS o
INNER JOIN employees AS e
ON o.employeeid = e.employeeid
WHERE o.orderdate = '9/5/1997'

SELECT lastname, employeeid
FROM employees AS e
WHERE EXISTS (SELECT * FROM orders AS o
WHERE e.employeeid = o.employeeid
AND o.orderdate = '9/5/97')

SELECT lastname, employeeid
FROM employees AS e
WHERE employeeid in (SELECT employeeid FROM orders AS o
WHERE o.orderdate = '9/5/97')






