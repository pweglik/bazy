-- 1. Wybierz nazwy i adresy wszystkich klientów mających siedziby w Londynie
select CompanyName, Country, City, PostalCode, Address from Customers
where City = 'London'

-- 2. Wybierz nazwy i adresy wszystkich klientów mających siedziby we Francji
-- lub w Hiszpanii
select CompanyName, Country, City, PostalCode, Address from Customers
where Country = 'France' or Country = 'Spain'

-- 3. Wybierz nazwy i ceny produktów o cenie jednostkowej pomiędzy 20.00 a 30.00
select ProductName, UnitPrice from Products
where UnitPrice between 20 and 30

-- 4. Wybierz nazwy i ceny produktów z kategorii ‘meat’
select CategoryID, CategoryName from Categories
where CategoryName = 'Meat/Poultry'

select ProductName, UnitPrice from Products
where CategoryID = 6

-- lub w jednym zapytaniu
select ProductName, UnitPrice from Products
where CategoryID = (
    select CategoryID from Categories
    where CategoryName = 'Meat/Poultry'
    )

-- 5. Wybierz nazwy produktów oraz inf. o stanie magazynu dla produktów
-- dostarczanych przez firmę ‘Tokyo Traders’
select SupplierID, CompanyName from Suppliers
where CompanyName = 'Tokyo Traders'

select ProductName, UnitsInStock from Products
where SupplierID = 4

-- lub w jednym zapytaniu
select ProductName, UnitsInStock from Products
where SupplierID = (
    select SupplierID from Suppliers
    where CompanyName = 'Tokyo Traders'
    )

-- 6. Wybierz nazwy produktów których nie ma w magazynie
select ProductName, UnitsInStock from Products
where UnitsInStock <= 0

