-- 1. Napisz polecenie, które oblicza wartość każdej pozycji zamówienia o numerze
-- 10250

select *, UnitPrice * Quantity * (1 - Discount) as wartosc from [Order Details]
where OrderID = 10250


-- 2. Napisz polecenie które dla każdego dostawcy (supplier) pokaże pojedynczą
-- kolumnę zawierającą nr telefonu i nr faksu w formacie
-- (numer telefonu i faksu mają być oddzielone przecinkiem)

select ISNULL(Phone, '')  + ISNULL(', ' + Fax, '') as ContactInfo from Suppliers

select concat(Phone, ', ' + Fax) as ContactInfo from Suppliers

