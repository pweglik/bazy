SELECT TOP 5 orderid, productid, quantity
FROM [order details]
ORDER BY quantity DESC

SELECT TOP 5 WITH TIES orderid, productid, quantity
FROM [order details]
ORDER BY quantity DESC

SELECT ReportsTo, *
FROM employees

SELECT COUNT (*)
FROM employees

SELECT COUNT(ReportsTo)
FROM employees

-- 1. Podaj liczbę produktów o cenach mniejszych niż 10$ lub większych niż
-- 20$

select count(*) from Products
where UnitPrice < 10 or UnitPrice > 20

-- 2. Podaj maksymalną cenę produktu dla produktów o cenach poniżej 20$

select top 1 UnitPrice from Products
where UnitPrice < 20
order by UnitPrice desc


select max(UnitPrice) from Products
where UnitPrice < 20

-- 3. Podaj maksymalną i minimalną i średnią cenę produktu dla produktów o
-- produktach sprzedawanych w butelkach (‘bottle’)

select top 1 UnitPrice from Products
where QuantityPerUnit like '%bottle%'
order by UnitPrice desc

select top 1 UnitPrice from Products
where QuantityPerUnit like '%bottle%'
order by UnitPrice asc

select max(UnitPrice), min(UnitPrice), avg(UnitPrice) from Products
where QuantityPerUnit like '%bottle%'

-- 4. Wypisz informację o wszystkich produktach o cenie powyżej średniej

select avg(UnitPrice) from Products

select * from Products
where UnitPrice > (select avg(UnitPrice) from Products)

-- 5. Podaj sumę/wartość zamówienia o numerze 10250

select sum(UnitPrice * Quantity * (1 - Discount)) from [Order Details]
where OrderID = 10250