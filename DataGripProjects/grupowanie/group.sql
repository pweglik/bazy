SELECT productid, orderid,quantity
FROM orderhist

SELECT productid ,SUM(quantity) AS total_quantity
FROM orderhist
GROUP BY productid

SELECT productid, SUM(quantity) AS total_quantity
FROM orderhist
WHERE productid = 2
GROUP BY productid

-- 1. Podaj maksymalną cenę zamawianego produktu dla każdego zamówienia

select OrderID, max(UnitPrice) from [Order Details]
group by OrderID

-- 2. Posortuj zamówienia wg maksymalnej ceny produktu

select OrderID, max(UnitPrice) from [Order Details]
group by OrderID
order by max(UnitPrice) desc

-- 3. Podaj maksymalną i minimalną cenę zamawianego produktu dla każdego
-- zamówienia

select OrderID, max(UnitPrice), min(UnitPrice) from [Order Details]
group by OrderID


-- 4. Podaj liczbę zamówień dostarczanych przez poszczególnych spedytorów
-- (przewoźników)
select ShipVia, count(*) from Orders
group by ShipVia


-- 5. Który z spedytorów był najaktywniejszy w 1997 roku
select top 1 ShipVia, count(*) as counter from Orders
where year(ShippedDate) = 1997
group by ShipVia
order by counter desc

---------------------------------------------------
SELECT productid, SUM(quantity) AS total_quantity
FROM orderhist
GROUP BY productid
HAVING SUM(quantity)>=30

SELECT productid, SUM(quantity) AS total_quantity
FROM [order details]
GROUP BY productid
HAVING SUM(quantity)>1200

-- 1. Wyświetl zamówienia dla których liczba pozycji zamówienia jest większa niż 5

select count(*), OrderID from [Order Details]
group by OrderID
having count(*) > 5

-- 2. Wyświetl klientów dla których w 1998 roku zrealizowano więcej niż 8 zamówień
-- (wyniki posortuj malejąco wg łącznej kwoty za dostarczenie zamówień dla
-- każdego z klientów)

select CustomerID, sum(Freight), count(*) from Orders
where year(ShippedDate) = 1998
group by CustomerId
having count(*) > 8
order by sum(Freight) desc

-- ZADANIA Z MICROSFOT TEAMS OD DR MARCJANA
-- 1.Ile lat przepracował w firmie każdy z pracowników?
select EmployeeID,(year(GETDATE()) - year(HireDate))
from Employees
order by (GETDATE() - HireDate)

-- 2.Policz sumę lat przepracowanych przez wszystkich pracowników i średni czas pracy w firmie
select sum(year(GETDATE()) - year(HireDate)),
       avg(year(GETDATE()) - year(HireDate))
from Employees

-- 3.Dla każdego pracownika wyświetl imię, nazwisko oraz wiek
select FirstName, LastName, (year(GETDATE()) - year(BirthDate)) as age
from Employees
order by age desc

-- 4.Policz średni wiek wszystkich pracowników
select avg(year(GETDATE()) - year(BirthDate)) as avg_age
from Employees

-- 5.Wyświetl wszystkich pracowników, którzy mają teraz więcej niż 65 lat.
select FirstName, LastName, (year(GETDATE()) - year(BirthDate)) as age
from Employees
where (year(GETDATE()) - year(BirthDate)) >= 66
order by age desc

-- 6.Policz średnią liczbę miesięcy przepracowanych przez każdego pracownika
select avg(datediff(month, HireDate, GETDATE()))
from Employees

-- 7.Wyświetl dane wszystkich pracowników, którzy przepracowali w firmie
-- co najmniej 320 miesięcy, ale nie więcej niż 333
select datediff(month, HireDate, GETDATE()), *
from Employees
where datediff(month, HireDate, GETDATE()) between 320 and 333



---------------------------------------------

SELECT productid, orderid, SUM(quantity) AS total_quantity
FROM orderhist
GROUP BY productid, orderid
WITH ROLLUP
ORDER BY productid, orderid

SELECT productid, orderid, SUM(quantity) AS total_quantity
FROM orderhist
GROUP BY productid, orderid
WITH CUBE
ORDER BY productid, orderid